# SATOG-API v1-alpha

#### PREREQUESITE

- docker
- make
- git
- ngrok
- postman (facultative)

#### PROJECT INSTALLATION

- clone project
- set or create '.env' file variables at project root (ex: .env.default)
- run: make
- run: make install
- run: make db
- go to http://localhost:82 for API/Symfony project
- go to http://localhost:83 for mongo-express interface
- go to http://localhost:84 for Jenkins interface

#### DEPENDENCIES INSTALLATION

- run: make php
- run: composer require "whatever..."

#### JENKINS INSTALLATION

- run: make jenkins
- run: cd var/jenkins_home/secrets
- run: cat initialAdminPassword
- copy password
- go to http://localhost:84 and paste
- install recommanded plugins
- create user
- go to http://localhost:84/pluginManager/ and download and install bitbucket plugin 
- restart jenkins (refresh the page if its taking too long)
- create the first job/item :
    - name : satog-api-dev
    - type : freestyle project
    - repository : https://Bitbucket_Username@bitbucket.org/chinacitizens/satog-api.git
    - branch : dev
    - checkbox: 'Build when a change is pushed to BitBucket' 
    - save and launch first build
- create the second job/item :
    - name : satog-api-staging
    - type : freestyle project
    - repository : https://Bitbucket_Username@bitbucket.org/chinacitizens/satog-api.git
    - branch : dev
    - checkbox: 'Build when a change is pushed to BitBucket' 
    - save and launch first build
- create the third job/item :
    - name : satog-api-prod
    - type : freestyle project
    - repository : https://Bitbucket_Username@bitbucket.org/chinacitizens/satog-api.git
    - branch : dev
    - checkbox: 'Build when a change is pushed to BitBucket' 
    - save and launch first build

### CODING PROCESS
(To do everytime the computer or ngrok restarts !!)

- start containers
- run: ngrok http 84
- go to https://bitbucket.org/chinacitizens/satog-api/admin/addon/admin/bitbucket-webhooks/bb-webhooks-repo-admin
- add new webhook :
    - title : satog-api-jenkins
    - URL : http://{HASH.ngrok.io}/bitbucket-hook/
    - checkbox : Skip certificate verification
- start coding
- write unit tests
- merge your branch on dev and go to http://localhost:84/job/satog-api-dev to check the tests results for any regression
- correct bugs on your branch (if any) and merge again on dev

#### TOOLS

See Makefile