<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/territory")
 */
class TerritoryController extends AbstractController
{
    /**
     * @Route("/")
     */
    public function index()
    {
        return new JsonResponse(array('controller' => 'territory'));
    }
}