<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/social")
 */
class SocialController extends AbstractController
{
    /**
     * @Route("/")
     */
    public function index()
    {
        return new JsonResponse(array('controller' => 'social'));
    }
}