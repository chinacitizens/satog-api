<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * @Route("/core")
 */
class CoreController extends AbstractController
{
    /**
     * @Route("/")
     */
    public function index()
    {
        return new JsonResponse(array('controller' => 'core'));
    }

    /**
     * @Route("/test")
     */
    public function test(DocumentManager $dm)
    {
        $serializer = new Serializer([new ObjectNormalizer()], [new JsonEncoder()]);

        $users = $dm
            ->getRepository('App:Member')
            ->findAll();

        $response = new Response();
        $response->setContent(
            $serializer->serialize($users, 'json')
        );
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        return $response;
    }
}
