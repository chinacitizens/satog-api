<?php

namespace App\DataFixtures\MongoDB;

use App\Document\Member;
use App\DataFixtures\MongoDB\JsonTools;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class MemberFixtures extends AbstractFixture implements DependentFixtureInterface
{
    /**
     * Loads json data
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $jsonTools = new JsonTools();

        foreach ($jsonTools->getArray('members') as $key => $data) {
            $member = new Member();
            $user = $this->getReference('user_' . rand(0, $jsonTools->getLastIndex('users')));
            $faction = $this->getReference('faction_' . rand(0, $jsonTools->getLastIndex('factions')));
            $territory = $this->getReference('territory_' . rand(0, $jsonTools->getLastIndex('territories')));
            $cart = $this->getReference('cart_' . $key);

            $member->setPicture($data->picture);
            $member->setLevel($data->level);
            $member->setExperience($data->experience);
            $member->setAttack($data->attack);
            $member->setDefense($data->defense);
            $member->setUser($user);
            $member->setFaction($faction);
            $member->setTerritory($territory);
            $member->setCart($cart);

            // add multiple random stalls
            for ($i = 0; $i < rand(0, 5); $i++) {
                $stall = $this->getReference('stall_' . rand(0, $jsonTools->getLastIndex('stalls')));
                $member->getStalls()->add($stall);
            }

            // add multiple random orders
            for ($i = 0; $i < rand(0, 5); $i++) {
                $order = $this->getReference('order_' . rand(0, $jsonTools->getLastIndex('orders')));
                $member->getOrders()->add($order);
            }

            // add multiple random transactions
            for ($i = 0; $i < rand(0, 5); $i++) {
                $transaction = $this->getReference('transaction_' . rand(0, $jsonTools->getLastIndex('transactions')));
                $member->getTransactions()->add($transaction);
            }

            $manager->persist($user);
            $manager->persist($territory);
            $manager->persist($cart);

            if (isset($stall)) {
                $manager->persist($stall);
            }

            if (isset($order)) {
                $manager->persist($order);
            }

            if (isset($transaction)) {
                $manager->persist($transaction);
            }

            $manager->persist($member);

            $this->addReference('member_' . $key, $member);
        }

        $manager->flush();
    }

    /**
     * @return array
     */
    public function getDependencies()
    {
        return array(
            UserFixtures::class,
            FactionFixtures::class,
            TerritoryFixtures::class,
            CartFixtures::class,
            StallFixtures::class,
            OrderFixtures::class,
            TransactionFixtures::class
        );
    }
}