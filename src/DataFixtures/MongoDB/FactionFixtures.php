<?php

namespace App\DataFixtures\MongoDB;

use App\Document\Faction;
use App\DataFixtures\MongoDB\JsonTools;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;

class FactionFixtures extends AbstractFixture
{
    /**
     * Loads json data
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $jsonTools = new JsonTools();

        foreach ($jsonTools->getArray('factions') as $key => $data) {
            $faction = new Faction();

            $faction->setName($data->name);
            $faction->setPicture($data->picture);
            $faction->setDescription($data->description);
            $faction->setBonusGold($data->bonus_gold);
            $faction->setBonusWood($data->bonus_wood);
            $faction->setBonusStone($data->bonus_stone);
            $faction->setBonusFood($data->bonus_food);

            $manager->persist($faction);

            $this->addReference('faction_' . $key, $faction);
        }

        $manager->flush();
    }
}