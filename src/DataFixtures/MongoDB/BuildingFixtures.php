<?php

namespace App\DataFixtures\MongoDB;

use App\Document\Building;
use App\DataFixtures\MongoDB\JsonTools;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;

class BuildingFixtures extends AbstractFixture
{
    /**
     * Loads json data
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $jsonTools = new JsonTools();

        foreach ($jsonTools->getArray('buildings') as $key => $data) {
            $building = new Building();

            $building->setName($data->name);
            $building->setPicture($data->picture);
            $building->setDescription($data->description);
            $building->setType($data->type);
            $building->setAttack($data->attack);
            $building->setDefense($data->defense);
            $building->setDamageMin($data->damage_min);
            $building->setDamageMax($data->damage_max);
            $building->setLife($data->life);
            $building->setGold($data->gold);
            $building->setWood($data->wood);
            $building->setStone($data->stone);
            $building->setFood($data->food);
            $building->setPopulation($data->population);
            $building->setPriceGold($data->price_gold);
            $building->setPriceWood($data->price_wood);
            $building->setPriceStone($data->price_stone);

            $manager->persist($building);

            $this->addReference('building_' . $key, $building);
        }

        $manager->flush();
    }
}