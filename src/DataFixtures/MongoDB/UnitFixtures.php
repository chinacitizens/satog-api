<?php

namespace App\DataFixtures\MongoDB;

use App\Document\Unit;
use App\DataFixtures\MongoDB\JsonTools;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;

class UnitFixtures extends AbstractFixture
{
    /**
     * Loads json data
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $jsonTools = new JsonTools();

        foreach ($jsonTools->getArray('units') as $key => $data) {
            $unit = new Unit();

            $unit->setName($data->name);
            $unit->setPicture($data->picture);
            $unit->setDescription($data->description);
            $unit->setType($data->type);
            $unit->setAttack($data->attack);
            $unit->setArmor($data->armor);
            $unit->setDamageMin($data->damage_min);
            $unit->setDamageMax($data->damage_max);
            $unit->setLife($data->life);
            $unit->setPriceGold($data->price_gold);
            $unit->setPricePopulation($data->price_population);
            $unit->setPriceFood($data->price_food);

            $manager->persist($unit);

            $this->addReference('unit_' . $key, $unit);
        }

        $manager->flush();
    }
}