<?php

namespace App\DataFixtures\MongoDB;

use App\Document\Cart;
use App\DataFixtures\MongoDB\JsonTools;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class CartFixtures extends AbstractFixture implements DependentFixtureInterface
{
    /**
     * Loads json data
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $jsonTools = new JsonTools();

        foreach ($jsonTools->getArray('carts') as $key => $data) {
            $cart = new Cart();

            $cart->setAp($data->ap);
            $cart->setGold($data->gold);
            $cart->setWood($data->wood);
            $cart->setStone($data->stone);
            $cart->setFood($data->food);

            // add multiple random items
            for ($i = 0; $i < rand(0, 5); $i++) {
                $cart->getItems()->add($this->getReference('item_' . rand(0, $jsonTools->getLastIndex('items'))));
            }

            $cart->setPrice($data->price);

            $manager->persist($cart);

            $this->addReference('cart_' . $key, $cart);
        }

        $manager->flush();
    }

    /**
     * @return array
     */
    public function getDependencies()
    {
        return array(
            ItemFixtures::class
        );
    }
}