<?php

namespace App\DataFixtures\MongoDB;

use App\Document\Order;
use App\DataFixtures\MongoDB\JsonTools;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class OrderFixtures extends AbstractFixture implements DependentFixtureInterface
{
    /**
     * Loads json data
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $jsonTools = new JsonTools();

        foreach ($jsonTools->getArray('orders') as $key => $data) {
            $order = new Order();

            $order->setAp($data->ap);
            $order->setGold($data->gold);
            $order->setWood($data->wood);
            $order->setStone($data->stone);
            $order->setFood($data->food);

            // add multiple random items
            for ($i = 0; $i < rand(0, 5); $i++) {
                $order->getItems()->add($this->getReference('item_' . rand(0, $jsonTools->getLastIndex('items'))));
            }

            $order->setPrice($data->price);
            $order->setStatus($data->status);

            $manager->persist($order);

            $this->addReference('order_' . $key, $order);
        }

        $manager->flush();
    }

    /**
     * @return array
     */
    public function getDependencies()
    {
        return array(
            ItemFixtures::class
        );
    }
}