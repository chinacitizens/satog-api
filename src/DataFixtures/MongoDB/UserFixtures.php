<?php

namespace App\DataFixtures\MongoDB;

use App\Document\User;
use App\DataFixtures\MongoDB\JsonTools;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;

class UserFixtures extends AbstractFixture implements ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * Loads json data
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $jsonTools = new JsonTools();

        foreach ($jsonTools->getArray('users') as $key => $data) {
            $user = new User();

            $user->setEmail($data->email);
            $user->setPassword($this->container->get('security.password_encoder')->encodePassword(
                $user,
                $data->password
            ));
            $user->setRoles($data->roles);

            $manager->persist($user);

            $this->setReference('user_' . $key, $user);
        }

        $manager->flush();
    }
}