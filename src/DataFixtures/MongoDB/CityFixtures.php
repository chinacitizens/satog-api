<?php

namespace App\DataFixtures\MongoDB;

use App\Document\City;
use App\DataFixtures\MongoDB\JsonTools;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class CityFixtures extends AbstractFixture implements DependentFixtureInterface
{
    /**
     * Loads json data
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $jsonTools = new JsonTools();

        foreach ($jsonTools->getArray('cities') as $key => $data) {
            $city = new City();
            $city->setName($data->name);
            $city->setPicture($data->picture);

            // add multiple random buildings
            for ($i = 0; $i < rand(0, 10); $i++) {
                $city->getBuildings()->add($this->getReference('building_' . rand(0, $jsonTools->getLastIndex('buildings'))));
            }

            // add multiple random units
            for ($i = 0; $i < rand(0, 10); $i++) {
                $city->getUnits()->add($this->getReference('unit_' . rand(0, $jsonTools->getLastIndex('units'))));
            }

            $manager->persist($city);

            $this->addReference('city_' . $key, $city);
        }

        $manager->flush();
    }

    /**
     * @return array
     */
    public function getDependencies()
    {
        return array(
            BuildingFixtures::class,
            UnitFixtures::class
        );
    }
}