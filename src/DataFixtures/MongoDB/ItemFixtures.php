<?php

namespace App\DataFixtures\MongoDB;

use App\Document\Item;
use App\DataFixtures\MongoDB\JsonTools;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;

class ItemFixtures extends AbstractFixture
{
    /**
     * Loads json data
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $jsonTools = new JsonTools();

        foreach ($jsonTools->getArray('items') as $key => $data) {
            $item = new Item();

            $item->setName($data->name);
            $item->setPicture($data->picture);
            $item->setDescription($data->description);
            $item->setBonusAttack($data->bonus_attack);
            $item->setBonusDefense($data->bonus_defense);
            $item->setPriceGold($data->price_gold);

            $manager->persist($item);

            $this->addReference('item_' . $key, $item);
        }

        $manager->flush();
    }
}