<?php

namespace App\DataFixtures\MongoDB;

use App\Document\Wallet;
use App\DataFixtures\MongoDB\JsonTools;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class WalletFixtures extends AbstractFixture implements DependentFixtureInterface
{
    /**
     * Loads json data
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $jsonTools = new JsonTools();

        foreach ($jsonTools->getArray('wallets') as $key => $data) {
            $wallet = new Wallet();

            $wallet->setAp($data->ap);
            $wallet->setGold($data->gold);
            $wallet->setWood($data->wood);
            $wallet->setStone($data->stone);
            $wallet->setFood($data->food);
            $wallet->setPopulation($data->population);

            // add multiple random items
            for ($i = 0; $i < rand(0, 5); $i++) {
                $wallet->getItems()->add($this->getReference('item_' . rand(0, $jsonTools->getLastIndex('items'))));
            }

            $manager->persist($wallet);

            $this->addReference('wallet_' . $key, $wallet);
        }

        $manager->flush();
    }

    /**
     * @return array
     */
    public function getDependencies()
    {
        return array(
            ItemFixtures::class
        );
    }
}