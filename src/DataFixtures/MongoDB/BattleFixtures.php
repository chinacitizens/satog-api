<?php

namespace App\DataFixtures\MongoDB;

use App\Document\Battle;
use App\DataFixtures\MongoDB\JsonTools;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class BattleFixtures extends AbstractFixture implements DependentFixtureInterface
{
    /**
     * Loads json data
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $jsonTools = new JsonTools();

        foreach ($jsonTools->getArray('battles') as $key => $data) {
            $battle = new Battle();
            $winner = $this->getReference('member_' . rand(0, $jsonTools->getLastIndex('members')));
            $attacker = $this->getReference('member_' . rand(0, $jsonTools->getLastIndex('members')));
            $defender = $this->getReference('member_' . rand(0, $jsonTools->getLastIndex('members')));

            $battle->setWinner($winner);
            $battle->setAttacker($attacker);
            $battle->setDefender($defender);

            $manager->persist($battle);

            $this->addReference('battle_' . $key, $battle);
        }

        $manager->flush();
    }

    /**
     * @return array
     */
    public function getDependencies()
    {
        return array(
            MemberFixtures::class
        );
    }
}