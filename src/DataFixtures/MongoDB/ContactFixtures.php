<?php

namespace App\DataFixtures\MongoDB;

use App\Document\Contact;
use App\DataFixtures\MongoDB\JsonTools;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;

class ContactFixtures extends AbstractFixture
{
    /**
     * Loads json data
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $jsonTools = new JsonTools();

        foreach ($jsonTools->getArray('contacts') as $key => $data) {
            $contact = new Contact();

            $contact->setSubject($data->subject);
            $contact->setEmail($data->email);
            $contact->setMessage($data->message);

            $manager->persist($contact);

            $this->addReference('contact_' . $key, $contact);
        }

        $manager->flush();
    }
}