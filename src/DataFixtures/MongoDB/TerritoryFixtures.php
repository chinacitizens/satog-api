<?php

namespace App\DataFixtures\MongoDB;

use App\Document\Territory;
use App\DataFixtures\MongoDB\JsonTools;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class TerritoryFixtures extends AbstractFixture implements DependentFixtureInterface
{
    /**
     * Loads json data
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $jsonTools = new JsonTools();

        foreach ($jsonTools->getArray('territories') as $key => $data) {
            $territory = new Territory();
            $wallet = $this->getReference('wallet_' . rand(0, $jsonTools->getLastIndex('wallets')));

            $territory->setName($data->name);
            $territory->setPicture($data->picture);

            // add multiple random cities
            for ($i = 0; $i < rand(0, 3); $i++) {
                $city = $this->getReference('city_' . rand(0, $jsonTools->getLastIndex('cities')));
                $territory->getCities()->add($city);
            }

            $territory->setWallet($wallet);

            if (isset($city)) {
                $manager->persist($city);
            }

            $manager->persist($wallet);
            $manager->persist($territory);

            $this->addReference('territory_' . $key, $territory);
        }

        $manager->flush();
    }

    /**
     * @return array
     */
    public function getDependencies()
    {
        return array(
            CityFixtures::class,
            WalletFixtures::class
        );
    }
}