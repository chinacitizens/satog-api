<?php

namespace App\DataFixtures\MongoDB;

class JsonTools
{
    /**
     * Returns the last index of a json file
     *
     * @return int
     */
    public function getLastIndex($fileName) {
        $array = $this->getArray($fileName);

        return array_key_last($array);
    }

    /**
     * Decodes and returns an array of a json file
     *
     * @return array
     */
    public function getArray($fileName) {
        $json = file_get_contents(__DIR__ . '/data/' . $fileName . '.json');
        $array = json_decode($json);

        return $array;
    }
}