<?php

namespace App\DataFixtures\MongoDB;

use App\Document\Transaction;
use App\DataFixtures\MongoDB\JsonTools;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;

class TransactionFixtures extends AbstractFixture
{
    /**
     * Loads json data
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $jsonTools = new JsonTools();

        foreach ($jsonTools->getArray('transactions') as $key => $data) {
            $transaction = new Transaction();

            $transaction->setTransactionId($data->transaction_id);
            $transaction->setAmount($data->amount);
            $transaction->setPaymentType($data->payment_type);
            $transaction->setStatus($data->status);

            $manager->persist($transaction);

            $this->addReference('transaction_' . $key, $transaction);
        }

        $manager->flush();
    }
}