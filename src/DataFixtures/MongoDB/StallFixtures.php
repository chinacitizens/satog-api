<?php

namespace App\DataFixtures\MongoDB;

use App\Document\Stall;
use App\DataFixtures\MongoDB\JsonTools;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;

class StallFixtures extends AbstractFixture
{
    /**
     * Loads json data
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $jsonTools = new JsonTools();

        foreach ($jsonTools->getArray('stalls') as $key => $data) {
            $stall = new Stall();

            $stall->setTitle($data->title);
            $stall->setOfferGold($data->offer_gold);
            $stall->setOfferWood($data->offer_wood);
            $stall->setOfferStone($data->offer_stone);
            $stall->setOfferFood($data->offer_food);

            // add multiple random offer items
            for ($i = 0; $i < rand(0, 5); $i++) {
                $stall->getOfferItems()->add($this->getReference('item_' . rand(0, $jsonTools->getLastIndex('items'))));
            }

            $stall->setDemandGold($data->demand_gold);
            $stall->setDemandWood($data->demand_wood);
            $stall->setDemandStone($data->demand_stone);
            $stall->setDemandFood($data->demand_food);

            // add multiple random demand items
            for ($i = 0; $i < rand(0, 5); $i++) {
                $stall->getDemandItems()->add($this->getReference('item_' . rand(0, $jsonTools->getLastIndex('items'))));
            }

            $manager->persist($stall);

            $this->addReference('stall_' . $key, $stall);
        }

        $manager->flush();
    }
}