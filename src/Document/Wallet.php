<?php

namespace App\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\Document
 * @ODM\HasLifecycleCallbacks
 */
class Wallet
{
    /**
     * @ODM\Id
     */
    protected $id;

    /**
     * @ODM\Field(type="int")
     */
    protected $ap;

    /**
     * @ODM\Field(type="int")
     */
    protected $gold;

    /**
     * @ODM\Field(type="int")
     */
    protected $wood;

    /**
     * @ODM\Field(type="int")
     */
    protected $stone;

    /**
     * @ODM\Field(type="int")
     */
    protected $food;

    /**
     * @ODM\Field(type="int")
     */
    protected $population;

    /**
     * @ODM\ReferenceMany(targetDocument="Item")
     */
    protected $items;

    /**
     * @ODM\Field(type="date")
     */
    protected $updatedAt;

    /**
     * @ODM\Field(type="date")
     */
    protected $createdAt;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    /**
     * @return object
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getAp()
    {
        return $this->ap;
    }

    /**
     * @param int $ap
     * @return Wallet
     */
    public function setAp(int $ap)
    {
        $this->ap = $ap;
        return $this;
    }

    /**
     * @return int
     */
    public function getGold()
    {
        return $this->gold;
    }

    /**
     * @param int $gold
     * @return Wallet
     */
    public function setGold(int $gold)
    {
        $this->gold = $gold;
        return $this;
    }

    /**
     * @return int
     */
    public function getWood()
    {
        return $this->wood;
    }

    /**
     * @param int $wood
     * @return Wallet
     */
    public function setWood(int $wood)
    {
        $this->wood = $wood;
        return $this;
    }

    /**
     * @return int
     */
    public function getStone()
    {
        return $this->stone;
    }

    /**
     * @param int $stone
     * @return Wallet
     */
    public function setStone(int $stone)
    {
        $this->stone = $stone;
        return $this;
    }

    /**
     * @return int
     */
    public function getFood()
    {
        return $this->food;
    }

    /**
     * @param int $food
     * @return Wallet
     */
    public function setFood(int $food)
    {
        $this->food = $food;
        return $this;
    }

    /**
     * @return int
     */
    public function getPopulation()
    {
        return $this->population;
    }

    /**
     * @param int $population
     * @return Wallet
     */
    public function setPopulation(int $population)
    {
        $this->population = $population;
        return $this;
    }

    /**
     * @return ArrayCollection|Item
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param Item $item
     * @return Wallet
     */
    public function addItem(?Item $item)
    {
        $this->items->add($item);
        return $this;
    }

    /**
     * @param Item $item
     * @return Wallet
     */
    public function removeItem(?Item $item)
    {
        $this->items->remove($item);
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @ODM\PrePersist()
     * @ODM\PreUpdate()
     */
    public function setUpdatedAt()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @ODM\PrePersist()
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();
    }
}