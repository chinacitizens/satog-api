<?php

namespace App\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\Document
 * @ODM\HasLifecycleCallbacks
 */
class Order
{
    const ABORTED = 'AB';
    const PENDING = 'PE';
    const PAYED   = 'PA';

    /**
     * @ODM\Id
     */
    protected $id;

    /**
     * @ODM\Field(type="int")
     */
    protected $ap;

    /**
     * @ODM\Field(type="int")
     */
    protected $gold;

    /**
     * @ODM\Field(type="int")
     */
    protected $wood;

    /**
     * @ODM\Field(type="int")
     */
    protected $stone;

    /**
     * @ODM\Field(type="int")
     */
    protected $food;

    /**
     * @ODM\ReferenceMany(targetDocument="Item")
     */
    protected $items;

    /**
     * @ODM\Field(type="int")
     */
    protected $price;

    /**
     * @ODM\Field(type="string")
     */
    protected $status;

    /**
     * @ODM\Field(type="date")
     */
    protected $updatedAt;

    /**
     * @ODM\Field(type="date")
     */
    protected $createdAt;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    /**
     * @return object
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getAp()
    {
        return $this->ap;
    }

    /**
     * @param int $ap
     * @return Order
     */
    public function setAp(int $ap)
    {
        $this->ap = $ap;
        return $this;
    }

    /**
     * @return int
     */
    public function getGold()
    {
        return $this->gold;
    }

    /**
     * @param int $gold
     * @return Order
     */
    public function setGold(int $gold)
    {
        $this->gold = $gold;
        return $this;
    }

    /**
     * @return int
     */
    public function getWood()
    {
        return $this->wood;
    }

    /**
     * @param int $wood
     * @return Order
     */
    public function setWood(int $wood)
    {
        $this->wood = $wood;
        return $this;
    }

    /**
     * @return int
     */
    public function getStone()
    {
        return $this->stone;
    }

    /**
     * @param int $stone
     * @return Order
     */
    public function setStone(int $stone)
    {
        $this->stone = $stone;
        return $this;
    }

    /**
     * @return int
     */
    public function getFood()
    {
        return $this->food;
    }

    /**
     * @param int $food
     * @return Order
     */
    public function setFood(int $food)
    {
        $this->food = $food;
        return $this;
    }

    /**
     * @return ArrayCollection|Item
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param Item $item
     * @return Order
     */
    public function addItem(Item $item)
    {
        $this->items->add($item);
        return $this;
    }

    /**
     * @param Item $item
     * @return Order
     */
    public function removeItem(Item $item)
    {
        $this->items->remove($item);
        return $this;
    }

    /**
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param int $price
     * @return Order
     */
    public function setPrice(int $price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return Order
     */
    public function setStatus(string $status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @ODM\PrePersist()
     * @ODM\PreUpdate()
     */
    public function setUpdatedAt()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @ODM\PrePersist()
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();
    }
}