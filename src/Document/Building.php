<?php

namespace App\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\Document
 * @ODM\HasLifecycleCallbacks
 */
class Building
{
    const DEFENSIVE = 'DE';
    const OFFENSIVE = 'OF';
    const SUPPLY    = 'SU';

    /**
     * @ODM\Id
     */
    protected $id;

    /**
     * @ODM\Field(type="string")
     */
    protected $name;

    /**
     * @ODM\Field(type="string")
     */
    protected $picture;

    /**
     * @ODM\Field(type="string")
     */
    protected $description;

    /**
     * @ODM\Field(type="string")
     */
    protected $type;

    /**
     * @ODM\Field(type="int")
     */
    protected $attack;

    /**
     * @ODM\Field(type="int")
     */
    protected $defense;

    /**
     * @ODM\Field(type="int")
     */
    protected $damageMin;

    /**
     * @ODM\Field(type="int")
     */
    protected $damageMax;

    /**
     * @ODM\Field(type="int")
     */
    protected $life;

    /**
     * @ODM\Field(type="int")
     */
    protected $gold;

    /**
     * @ODM\Field(type="int")
     */
    protected $wood;

    /**
     * @ODM\Field(type="int")
     */
    protected $stone;

    /**
     * @ODM\Field(type="int")
     */
    protected $food;

    /**
     * @ODM\Field(type="int")
     */
    protected $population;

    /**
     * @ODM\Field(type="int")
     */
    protected $priceGold;

    /**
     * @ODM\Field(type="int")
     */
    protected $priceWood;

    /**
     * @ODM\Field(type="int")
     */
    protected $priceStone;

    /**
     * @ODM\Field(type="date")
     */
    protected $updatedAt;

    /**
     * @ODM\Field(type="date")
     */
    protected $createdAt;

    /**
     * @return object
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Building
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @param string $picture
     * @return Building
     */
    public function setPicture(string $picture)
    {
        $this->picture = $picture;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Building
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Building
     */
    public function setType(string $type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return int
     */
    public function getAttack()
    {
        return $this->attack;
    }

    /**
     * @param int $attack
     * @return Building
     */
    public function setAttack(int $attack)
    {
        $this->attack = $attack;
        return $this;
    }

    /**
     * @return int
     */
    public function getDefense()
    {
        return $this->defense;
    }

    /**
     * @param int $defense
     * @return Building
     */
    public function setDefense(int $defense)
    {
        $this->defense = $defense;
        return $this;
    }

    /**
     * @return int
     */
    public function getDamageMin()
    {
        return $this->damageMin;
    }

    /**
     * @param int $damageMin
     * @return Building
     */
    public function setDamageMin(int $damageMin)
    {
        $this->damageMin = $damageMin;
        return $this;
    }

    /**
     * @return int
     */
    public function getDamageMax()
    {
        return $this->damageMax;
    }

    /**
     * @param int $damageMax
     * @return Building
     */
    public function setDamageMax(int $damageMax)
    {
        $this->damageMax = $damageMax;
        return $this;
    }

    /**
     * @return int
     */
    public function getLife()
    {
        return $this->life;
    }

    /**
     * @param int $life
     * @return Building
     */
    public function setLife(int $life)
    {
        $this->life = $life;
        return $this;
    }

    /**
     * @return int
     */
    public function getGold()
    {
        return $this->gold;
    }

    /**
     * @param int $gold
     * @return Building
     */
    public function setGold(int $gold)
    {
        $this->gold = $gold;
        return $this;
    }

    /**
     * @return int
     */
    public function getWood()
    {
        return $this->wood;
    }

    /**
     * @param int $wood
     * @return Building
     */
    public function setWood(int $wood)
    {
        $this->wood = $wood;
        return $this;
    }

    /**
     * @return int
     */
    public function getStone()
    {
        return $this->stone;
    }

    /**
     * @param int $stone
     * @return Building
     */
    public function setStone(int $stone)
    {
        $this->stone = $stone;
        return $this;
    }

    /**
     * @return int
     */
    public function getFood()
    {
        return $this->food;
    }

    /**
     * @param int $food
     * @return Building
     */
    public function setFood(int $food)
    {
        $this->food = $food;
        return $this;
    }

    /**
     * @return int
     */
    public function getPopulation()
    {
        return $this->population;
    }

    /**
     * @param int $population
     * @return Building
     */
    public function setPopulation(int $population)
    {
        $this->population = $population;
        return $this;
    }

    /**
     * @return int
     */
    public function getPriceGold()
    {
        return $this->priceGold;
    }

    /**
     * @param int $priceGold
     * @return Building
     */
    public function setPriceGold(int $priceGold)
    {
        $this->priceGold = $priceGold;
        return $this;
    }

    /**
     * @return int
     */
    public function getPriceWood()
    {
        return $this->priceWood;
    }

    /**
     * @param int $priceWood
     * @return Building
     */
    public function setPriceWood(int $priceWood)
    {
        $this->priceWood = $priceWood;
        return $this;
    }

    /**
     * @return int
     */
    public function getPriceStone()
    {
        return $this->priceStone;
    }

    /**
     * @param int $priceStone
     * @return Building
     */
    public function setPriceStone(int $priceStone)
    {
        $this->priceStone = $priceStone;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @ODM\PrePersist()
     * @ODM\PreUpdate()
     */
    public function setUpdatedAt()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @ODM\PrePersist()
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();
    }
}