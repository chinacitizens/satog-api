<?php

namespace App\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\Document
 * @ODM\HasLifecycleCallbacks
 */
class Member
{
    /**
     * @ODM\Id
     */
    protected $id;

    /**
     * @ODM\Field(type="string")
     */
    protected $picture;

    /**
     * @ODM\Field(type="int")
     */
    protected $level;

    /**
     * @ODM\Field(type="int")
     */
    protected $experience;

    /**
     * @ODM\Field(type="int")
     */
    protected $attack;

    /**
     * @ODM\Field(type="int")
     */
    protected $defense;

    /**
     * @ODM\ReferenceOne(targetDocument="User")
     */
    protected $user;

    /**
     * @ODM\ReferenceOne(targetDocument="Faction")
     */
    protected $faction;

    /**
     * @ODM\ReferenceOne(targetDocument="Territory")
     */
    protected $territory;

    /**
     * @ODM\ReferenceMany(targetDocument="Stall")
     */
    protected $stalls;

    /**
     * @ODM\ReferenceOne(targetDocument="Cart")
     */
    protected $cart;

    /**
     * @ODM\ReferenceMany(targetDocument="Order")
     */
    protected $orders;

    /**
     * @ODM\ReferenceMany(targetDocument="Transaction")
     */
    protected $transactions;

    /**
     * @ODM\Field(type="date")
     */
    protected $updatedAt;

    /**
     * @ODM\Field(type="date")
     */
    protected $createdAt;

    public function __construct()
    {
        $this->stalls = new ArrayCollection();
        $this->orders = new ArrayCollection();
        $this->transactions = new ArrayCollection();
    }

    /**
     * @return object
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @param string $picture
     * @return Member
     */
    public function setPicture(string $picture)
    {
        $this->picture = $picture;
        return $this;
    }

    /**
     * @return int
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @param int $level
     * @return Member
     */
    public function setLevel(int $level)
    {
        $this->level = $level;
        return $this;
    }

    /**
     * @return int
     */
    public function getExperience()
    {
        return $this->experience;
    }

    /**
     * @param int $experience
     * @return Member
     */
    public function setExperience(int $experience)
    {
        $this->experience = $experience;
        return $this;
    }

    /**
     * @return int
     */
    public function getAttack()
    {
        return $this->attack;
    }

    /**
     * @param int $attack
     * @return Member
     */
    public function setAttack(int $attack)
    {
        $this->attack = $attack;
        return $this;
    }

    /**
     * @return int
     */
    public function getDefense()
    {
        return $this->defense;
    }

    /**
     * @param int $defense
     * @return Member
     */
    public function setDefense(int $defense)
    {
        $this->defense = $defense;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param $user
     * @return Member
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Faction
     */
    public function getFaction()
    {
        return $this->faction;
    }

    /**
     * @param Faction $faction
     * @return Member
     */
    public function setFaction(?Faction $faction)
    {
        $this->faction = $faction;
        return $this;
    }

    /**
     * @return Territory
     */
    public function getTerritory()
    {
        return $this->territory;
    }

    /**
     * @param Territory $territory
     * @return Member
     */
    public function setTerritory(?Territory $territory)
    {
        $this->territory = $territory;
        return $this;
    }

    /**
     * @return ArrayCollection|Stall
     */
    public function getStalls()
    {
        return $this->stalls;
    }

    /**
     * @param Stall $stall
     * @return Member
     */
    public function addStall(?Stall $stall)
    {
        $this->stalls->add($stall);
        return $this;
    }

    /**
     * @param Stall $stall
     * @return Member
     */
    public function removeStall(?Stall $stall)
    {
        $this->stalls->remove($stall);
        return $this;
    }

    /**
     * @return Cart
     */
    public function getCart()
    {
        return $this->cart;
    }

    /**
     * @param Cart $cart
     * @return Member
     */
    public function setCart(?Cart $cart)
    {
        $this->cart = $cart;
        return $this;
    }

    /**
     * @return ArrayCollection|Order
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * @param Order $order
     * @return Member
     */
    public function addOrder(?Order $order)
    {
        $this->orders->add($order);
        return $this;
    }

    /**
     * @param Order $order
     * @return Member
     */
    public function removeOrder(?Order $order)
    {
        $this->orders->remove($order);
        return $this;
    }

    /**
     * @return ArrayCollection|Transaction
     */
    public function getTransactions()
    {
        return $this->transactions;
    }

    /**
     * @param Transaction $transaction
     * @return Member
     */
    public function addTransaction(?Transaction $transaction)
    {
        $this->transactions->add($transaction);
        return $this;
    }

    /**
     * @param Transaction $transaction
     * @return Member
     */
    public function removeTransaction(?Transaction $transaction)
    {
        $this->transactions->remove($transaction);
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @ODM\PrePersist()
     * @ODM\PreUpdate()
     */
    public function setUpdatedAt()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @ODM\PrePersist()
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();
    }
}