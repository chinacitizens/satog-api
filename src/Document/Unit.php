<?php

namespace App\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\Document
 * @ODM\HasLifecycleCallbacks
 */
class Unit
{
    const RANGE = 'RA';
    const MELEE = 'ME';
    const MAGIC = 'MA';

    /**
     * @ODM\Id
     */
    protected $id;

    /**
     * @ODM\Field(type="string")
     */
    protected $name;

    /**
     * @ODM\Field(type="string")
     */
    protected $picture;

    /**
     * @ODM\Field(type="string")
     */
    protected $description;

    /**
     * @ODM\Field(type="string")
     */
    protected $type;

    /**
     * @ODM\Field(type="int")
     */
    protected $attack;

    /**
     * @ODM\Field(type="int")
     */
    protected $armor;

    /**
     * @ODM\Field(type="int")
     */
    protected $damageMin;

    /**
     * @ODM\Field(type="int")
     */
    protected $damageMax;

    /**
     * @ODM\Field(type="int")
     */
    protected $life;

    /**
     * @ODM\Field(type="int")
     */
    protected $priceGold;

    /**
     * @ODM\Field(type="int")
     */
    protected $priceFood;

    /**
     * @ODM\Field(type="int")
     */
    protected $pricePopulation;

    /**
     * @ODM\Field(type="date")
     */
    protected $updatedAt;

    /**
     * @ODM\Field(type="date")
     */
    protected $createdAt;

    /**
     * @return object
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Unit
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @param string $picture
     * @return Unit
     */
    public function setPicture(string $picture)
    {
        $this->picture = $picture;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Unit
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Unit
     */
    public function setType(string $type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return int
     */
    public function getAttack()
    {
        return $this->attack;
    }

    /**
     * @param int $attack
     * @return Unit
     */
    public function setAttack(int $attack)
    {
        $this->attack = $attack;
        return $this;
    }

    /**
     * @return int
     */
    public function getArmor()
    {
        return $this->armor;
    }

    /**
     * @param int $armor
     * @return Unit
     */
    public function setArmor(int $armor)
    {
        $this->armor = $armor;
        return $this;
    }

    /**
     * @return int
     */
    public function getDamageMin()
    {
        return $this->damageMin;
    }

    /**
     * @param int $damageMin
     * @return Unit
     */
    public function setDamageMin(int $damageMin)
    {
        $this->damageMin = $damageMin;
        return $this;
    }

    /**
     * @return int
     */
    public function getDamageMax()
    {
        return $this->damageMax;
    }

    /**
     * @param int $damageMax
     * @return Unit
     */
    public function setDamageMax(int $damageMax)
    {
        $this->damageMax = $damageMax;
        return $this;
    }

    /**
     * @return int
     */
    public function getLife()
    {
        return $this->life;
    }

    /**
     * @param int $life
     * @return Unit
     */
    public function setLife(int $life)
    {
        $this->life = $life;
        return $this;
    }

    /**
     * @return int
     */
    public function getPriceGold()
    {
        return $this->priceGold;
    }

    /**
     * @param int $priceGold
     * @return Unit
     */
    public function setPriceGold(int $priceGold)
    {
        $this->priceGold = $priceGold;
        return $this;
    }

    /**
     * @return int
     */
    public function getPriceFood()
    {
        return $this->priceFood;
    }

    /**
     * @param int $priceFood
     * @return Unit
     */
    public function setPriceFood(int $priceFood)
    {
        $this->priceFood = $priceFood;
        return $this;
    }

    /**
     * @return int
     */
    public function getPricePopulation()
    {
        return $this->pricePopulation;
    }

    /**
     * @param int $pricePopulation
     * @return Unit
     */
    public function setPricePopulation(int $pricePopulation)
    {
        $this->pricePopulation = $pricePopulation;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @ODM\PrePersist()
     * @ODM\PreUpdate()
     */
    public function setUpdatedAt()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @ODM\PrePersist()
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();
    }
}