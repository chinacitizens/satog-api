<?php

namespace App\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\Document
 * @ODM\HasLifecycleCallbacks
 */
class City
{
    /**
     * @ODM\Id
     */
    protected $id;

    /**
     * @ODM\Field(type="string")
     */
    protected $name;

    /**
     * @ODM\Field(type="string")
     */
    protected $picture;

    /**
     * @ODM\ReferenceMany(targetDocument="Building")
     */
    protected $buildings;

    /**
     * @ODM\ReferenceMany(targetDocument="Unit")
     */
    protected $units;

    /**
     * @ODM\Field(type="date")
     */
    protected $updatedAt;

    /**
     * @ODM\Field(type="date")
     */
    protected $createdAt;

    public function __construct()
    {
        $this->buildings = new ArrayCollection();
        $this->units = new ArrayCollection();
    }

    /**
     * @return object
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return City
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @param string $picture
     * @return City
     */
    public function setPicture(string $picture)
    {
        $this->picture = $picture;
        return $this;
    }

    /**
     * @return ArrayCollection|Building
     */
    public function getBuildings()
    {
        return $this->buildings;
    }

    /**
     * @param Building $building
     * @return City
     */
    public function addBuilding(?Building $building)
    {
        $this->buildings->add($building);
        return $this;
    }

    /**
     * @param Building $building
     * @return City
     */
    public function removeBuilding(?Building $building)
    {
        $this->buildings->remove($building);
        return $this;
    }

    /**
     * @return ArrayCollection|Unit
     */
    public function getUnits()
    {
        return $this->units;
    }

    /**
     * @param Unit $unit
     * @return City
     */
    public function addUnit(?Unit $unit)
    {
        $this->units->add($unit);
        return $this;
    }

    /**
     * @param Unit $unit
     * @return City
     */
    public function removeUnit(?Unit $unit)
    {
        $this->units->remove($unit);
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @ODM\PrePersist()
     * @ODM\PreUpdate()
     */
    public function setUpdatedAt()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @ODM\PrePersist()
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();
    }
}