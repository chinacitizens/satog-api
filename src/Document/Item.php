<?php

namespace App\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\Document
 * @ODM\HasLifecycleCallbacks
 */
class Item
{
    /**
     * @ODM\Id
     */
    protected $id;

    /**
     * @ODM\Field(type="string")
     */
    protected $name;

    /**
     * @ODM\Field(type="string")
     */
    protected $picture;

    /**
     * @ODM\Field(type="string")
     */
    protected $description;

    /**
     * @ODM\Field(type="int")
     */
    protected $bonusAttack;

    /**
     * @ODM\Field(type="int")
     */
    protected $bonusDefense;

    /**
     * @ODM\Field(type="int")
     */
    protected $priceGold;

    /**
     * @ODM\Field(type="date")
     */
    protected $updatedAt;

    /**
     * @ODM\Field(type="date")
     */
    protected $createdAt;

    /**
     * @return object
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Item
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @param string $picture
     * @return Item
     */
    public function setPicture(string $picture)
    {
        $this->picture = $picture;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Item
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return int
     */
    public function getBonusAttack()
    {
        return $this->bonusAttack;
    }

    /**
     * @param int $bonusAttack
     * @return Item
     */
    public function setBonusAttack(int $bonusAttack)
    {
        $this->bonusAttack = $bonusAttack;
        return $this;
    }

    /**
     * @return int
     */
    public function getBonusDefense()
    {
        return $this->bonusDefense;
    }

    /**
     * @param int $bonusDefense
     * @return Item
     */
    public function setBonusDefense(int $bonusDefense)
    {
        $this->bonusDefense = $bonusDefense;
        return $this;
    }

    /**
     * @return int
     */
    public function getPriceGold()
    {
        return $this->priceGold;
    }

    /**
     * @param int $priceGold
     * @return Item
     */
    public function setPriceGold(int $priceGold)
    {
        $this->priceGold = $priceGold;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @ODM\PrePersist()
     * @ODM\PreUpdate()
     */
    public function setUpdatedAt()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @ODM\PrePersist()
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();
    }
}