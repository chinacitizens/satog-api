<?php

namespace App\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\Document
 * @ODM\HasLifecycleCallbacks
 */
class Territory
{
    /**
     * @ODM\Id
     */
    protected $id;

    /**
     * @ODM\Field(type="string")
     */
    protected $name;

    /**
     * @ODM\Field(type="string")
     */
    protected $picture;

    /**
     * @ODM\ReferenceMany(targetDocument="City")
     */
    protected $cities;

    /**
     * @ODM\ReferenceOne(targetDocument="Wallet")
     */
    protected $wallet;

    /**
     * @ODM\Field(type="date")
     */
    protected $updatedAt;

    /**
     * @ODM\Field(type="date")
     */
    protected $createdAt;

    public function __construct()
    {
        $this->cities = new ArrayCollection();
    }

    /**
     * @return object
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Territory
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @param string $picture
     * @return Territory
     */
    public function setPicture(string $picture)
    {
        $this->picture = $picture;
        return $this;
    }

    /**
     * @return ArrayCollection|City
     */
    public function getCities()
    {
        return $this->cities;
    }

    /**
     * @param City $city
     * @return Territory
     */
    public function addCity(?City $city)
    {
        $this->cities->add($city);
        return $this;
    }

    /**
     * @param City $city
     * @return Territory
     */
    public function removeCity(?City $city)
    {
        $this->cities->remove($city);
        return $this;
    }

    /**
     * @return Wallet
     */
    public function getWallet()
    {
        return $this->wallet;
    }

    /**
     * @param Wallet $wallet
     * @return Territory
     */
    public function setWallet(?Wallet $wallet)
    {
        $this->wallet = $wallet;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @ODM\PrePersist()
     * @ODM\PreUpdate()
     */
    public function setUpdatedAt()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @ODM\PrePersist()
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();
    }
}