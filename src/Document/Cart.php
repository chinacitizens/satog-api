<?php

namespace App\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\Document
 * @ODM\HasLifecycleCallbacks
 */
class Cart
{
    /**
     * @ODM\Id
     */
    protected $id;

    /**
     * @ODM\Field(type="int")
     */
    protected $ap;

    /**
     * @ODM\Field(type="int")
     */
    protected $gold;

    /**
     * @ODM\Field(type="int")
     */
    protected $wood;

    /**
     * @ODM\Field(type="int")
     */
    protected $stone;

    /**
     * @ODM\Field(type="int")
     */
    protected $food;

    /**
     * @ODM\ReferenceMany(targetDocument="Item")
     */
    protected $items;

    /**
     * @ODM\Field(type="int")
     */
    protected $price;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    /**
     * @return object
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getAp()
    {
        return $this->ap;
    }

    /**
     * @param int $ap
     * @return Cart
     */
    public function setAp(int $ap)
    {
        $this->ap = $ap;
        return $this;
    }

    /**
     * @return int
     */
    public function getGold()
    {
        return $this->gold;
    }

    /**
     * @param int $gold
     * @return Cart
     */
    public function setGold(int $gold)
    {
        $this->gold = $gold;
        return $this;
    }

    /**
     * @return int
     */
    public function getWood()
    {
        return $this->wood;
    }

    /**
     * @param int $wood
     * @return Cart
     */
    public function setWood(int $wood)
    {
        $this->wood = $wood;
        return $this;
    }

    /**
     * @return int
     */
    public function getStone()
    {
        return $this->stone;
    }

    /**
     * @param int $stone
     * @return Cart
     */
    public function setStone(int $stone)
    {
        $this->stone = $stone;
        return $this;
    }

    /**
     * @return int
     */
    public function getFood()
    {
        return $this->food;
    }

    /**
     * @param int $food
     * @return Cart
     */
    public function setFood(int $food)
    {
        $this->food = $food;
        return $this;
    }

    /**
     * @return ArrayCollection|Item
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param Item $item
     * @return Cart
     */
    public function addItem(Item $item)
    {
        $this->items->add($item);
        return $this;
    }

    /**
     * @param Item $item
     * @return Cart
     */
    public function removeItem(Item $item)
    {
        $this->items->remove($item);
        return $this;
    }

    /**
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param int $price
     * @return Cart
     */
    public function setPrice(int $price)
    {
        $this->price = $price;
        return $this;
    }
}