<?php

namespace App\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\Document
 * @ODM\HasLifecycleCallbacks
 */
class Transaction
{
    const FAILED  = 'FA';
    const SUCCEED = 'SU';

    /**
     * @ODM\Id
     */
    protected $id;

    /**
     * @ODM\Field(type="int")
     */
    protected $transaction_id;

    /**
     * @ODM\Field(type="int")
     */
    protected $amount;

    /**
     * @ODM\Field(type="string")
     */
    protected $payment_type;

    /**
     * @ODM\Field(type="string")
     */
    protected $status;

    /**
     * @ODM\Field(type="date")
     */
    protected $createdAt;

    /**
     * @return object
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTransactionId()
    {
        return $this->transaction_id;
    }

    /**
     * @param string $transaction_id
     * @return Transaction
     */
    public function setTransactionId(string $transaction_id)
    {
        $this->transaction_id = $transaction_id;
        return $this;
    }

    /**
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param int $amount
     * @return Transaction
     */
    public function setAmount(int $amount)
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return string
     */
    public function getPaymentType()
    {
        return $this->payment_type;
    }

    /**
     * @param string $payment_type
     * @return Transaction
     */
    public function setPaymentType(string $payment_type)
    {
        $this->payment_type = $payment_type;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return Transaction
     */
    public function setStatus(string $status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @ODM\PrePersist()
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();
    }
}