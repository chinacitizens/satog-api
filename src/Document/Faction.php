<?php

namespace App\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\Document
 * @ODM\HasLifecycleCallbacks
 */
class Faction
{
    /**
     * @ODM\Id
     */
    protected $id;

    /**
     * @ODM\Field(type="string")
     */
    protected $name;

    /**
     * @ODM\Field(type="string")
     */
    protected $picture;

    /**
     * @ODM\Field(type="string")
     */
    protected $description;

    /**
     * @ODM\Field(type="int")
     */
    protected $bonusGold;

    /**
     * @ODM\Field(type="int")
     */
    protected $bonusWood;

    /**
     * @ODM\Field(type="int")
     */
    protected $bonusStone;

    /**
     * @ODM\Field(type="int")
     */
    protected $bonusFood;

    /**
     * @ODM\Field(type="date")
     */
    protected $updatedAt;

    /**
     * @ODM\Field(type="date")
     */
    protected $createdAt;

    /**
     * @return object
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Faction
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @param string $picture
     * @return Faction
     */
    public function setPicture(string $picture)
    {
        $this->picture = $picture;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Faction
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return int
     */
    public function getBonusGold()
    {
        return $this->bonusGold;
    }

    /**
     * @param int $bonusGold
     * @return Faction
     */
    public function setBonusGold(int $bonusGold)
    {
        $this->bonusGold = $bonusGold;
        return $this;
    }

    /**
     * @return int
     */
    public function getBonusWood()
    {
        return $this->bonusWood;
    }

    /**
     * @param int $bonusWood
     * @return Faction
     */
    public function setBonusWood(int $bonusWood)
    {
        $this->bonusWood = $bonusWood;
        return $this;
    }

    /**
     * @return int
     */
    public function getBonusStone()
    {
        return $this->bonusStone;
    }

    /**
     * @param int $bonusStone
     * @return Faction
     */
    public function setBonusStone(int $bonusStone)
    {
        $this->bonusStone = $bonusStone;
        return $this;
    }

    /**
     * @return int
     */
    public function getBonusFood()
    {
        return $this->bonusFood;
    }

    /**
     * @param int $bonusFood
     * @return Faction
     */
    public function setBonusFood(int $bonusFood)
    {
        $this->bonusFood = $bonusFood;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @ODM\PrePersist()
     * @ODM\PreUpdate()
     */
    public function setUpdatedAt()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @ODM\PrePersist()
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();
    }
}