<?php

namespace App\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\Document
 * @ODM\HasLifecycleCallbacks
 */
class Battle
{
    /**
     * @ODM\Id
     */
    protected $id;

    /**
     * @ODM\ReferenceOne(targetDocument="Member")
     */
    protected $winner;

    /**
     * @ODM\ReferenceOne(targetDocument="Member")
     */
    protected $attacker;

    /**
     * @ODM\ReferenceOne(targetDocument="Member")
     */
    protected $defender;

    /**
     * @ODM\Field(type="date")
     */
    protected $createdAt;

    /**
     * @return object
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Member
     */
    public function getWinner()
    {
        return $this->winner;
    }

    /**
     * @param Member $winner
     * @return Battle
     */
    public function setWinner(?Member $winner)
    {
        $this->winner = $winner;
        return $this;
    }

    /**
     * @return Member
     */
    public function getAttacker()
    {
        return $this->attacker;
    }

    /**
     * @param Member $attacker
     * @return Battle
     */
    public function setAttacker(?Member $attacker)
    {
        $this->attacker = $attacker;
        return $this;
    }

    /**
     * @return Member
     */
    public function getDefender()
    {
        return $this->defender;
    }

    /**
     * @param Member $defender
     * @return Battle
     */
    public function setDefender(?Member $defender)
    {
        $this->defender = $defender;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @ODM\PrePersist()
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();
    }
}