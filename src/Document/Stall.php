<?php

namespace App\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\Document
 * @ODM\HasLifecycleCallbacks
 */
class Stall
{
    const ON_SALE = 'OS';
    const SOLD = 'SO';

    /**
     * @ODM\Id
     */
    protected $id;

    /**
     * @ODM\Field(type="string")
     */
    protected $title;

    /**
     * @ODM\Field(type="string")
     */
    protected $status;

    /**
     * @ODM\Field(type="int")
     */
    protected $offerGold;

    /**
     * @ODM\Field(type="int")
     */
    protected $offerWood;

    /**
     * @ODM\Field(type="int")
     */
    protected $offerStone;

    /**
     * @ODM\Field(type="int")
     */
    protected $offerFood;

    /**
     * @ODM\ReferenceMany(targetDocument="Item")
     */
    protected $offerItems;

    /**
     * @ODM\Field(type="int")
     */
    protected $demandGold;

    /**
     * @ODM\Field(type="int")
     */
    protected $demandWood;

    /**
     * @ODM\Field(type="int")
     */
    protected $demandStone;

    /**
     * @ODM\Field(type="int")
     */
    protected $demandFood;

    /**
     * @ODM\ReferenceMany(targetDocument="Item")
     */
    protected $demandItems;

    /**
     * @ODM\Field(type="date")
     */
    protected $updatedAt;

    /**
     * @ODM\Field(type="date")
     */
    protected $createdAt;

    public function __construct()
    {
        $this->offerItems = new ArrayCollection();
        $this->demandItems = new ArrayCollection();
    }

    /**
     * @return object
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Stall
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return Stall
     */
    public function setStatus(string $status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return int
     */
    public function getOfferGold()
    {
        return $this->offerGold;
    }

    /**
     * @param int $offerGold
     * @return Stall
     */
    public function setOfferGold(int $offerGold)
    {
        $this->offerGold = $offerGold;
        return $this;
    }

    /**
     * @return int
     */
    public function getOfferWood()
    {
        return $this->offerWood;
    }

    /**
     * @param int $offerWood
     * @return Stall
     */
    public function setOfferWood(int $offerWood)
    {
        $this->offerWood = $offerWood;
        return $this;
    }

    /**
     * @return int
     */
    public function getOfferStone()
    {
        return $this->offerStone;
    }

    /**
     * @param int $offerStone
     * @return Stall
     */
    public function setOfferStone(int $offerStone)
    {
        $this->offerStone = $offerStone;
        return $this;
    }

    /**
     * @return int
     */
    public function getOfferFood()
    {
        return $this->offerFood;
    }

    /**
     * @param int $offerFood
     * @return Stall
     */
    public function setOfferFood(int $offerFood)
    {
        $this->offerFood = $offerFood;
        return $this;
    }

    /**
     * @return ArrayCollection|Item
     */
    public function getOfferItems()
    {
        return $this->offerItems;
    }

    /**
     * @param Item $offerItem
     * @return Stall
     */
    public function addOfferItem(?Item $offerItem)
    {
        $this->offerItems->add($offerItem);
        return $this;
    }

    /**
     * @param Item $offerItem
     * @return Stall
     */
    public function removeOfferItem(?Item $offerItem)
    {
        $this->offerItems->remove($offerItem);
        return $this;
    }

    /**
     * @return int
     */
    public function getDemandGold()
    {
        return $this->demandGold;
    }

    /**
     * @param int $demandGold
     * @return Stall
     */
    public function setDemandGold(int $demandGold)
    {
        $this->demandGold = $demandGold;
        return $this;
    }

    /**
     * @return int
     */
    public function getDemandWood()
    {
        return $this->demandWood;
    }

    /**
     * @param int $demandWood
     * @return Stall
     */
    public function setDemandWood(int $demandWood)
    {
        $this->demandWood = $demandWood;
        return $this;
    }

    /**
     * @return int
     */
    public function getDemandStone()
    {
        return $this->demandStone;
    }

    /**
     * @param int $demandStone
     * @return Stall
     */
    public function setDemandStone(int $demandStone)
    {
        $this->demandStone = $demandStone;
        return $this;
    }

    /**
     * @return int
     */
    public function getDemandFood()
    {
        return $this->demandFood;
    }

    /**
     * @param int $demandFood
     * @return Stall
     */
    public function setDemandFood(int $demandFood)
    {
        $this->demandFood = $demandFood;
        return $this;
    }

    /**
     * @return ArrayCollection|Item
     */
    public function getDemandItems()
    {
        return $this->demandItems;
    }

    /**
     * @param Item $demandItem
     * @return Stall
     */
    public function addDemandItem(?Item $demandItem)
    {
        $this->demandItems->add($demandItem);
        return $this;
    }

    /**
     * @param Item $demandItem
     * @return Stall
     */
    public function removeDemandItem(?Item $demandItem)
    {
        $this->demandItems->remove($demandItem);
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @ODM\PrePersist()
     * @ODM\PreUpdate()
     */
    public function setUpdatedAt()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @ODM\PrePersist()
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();
    }
}