include .env

# build images and mount all docker containers
start:
	docker-compose up -d --build && docker image prune -a -f

# install vendor dependencies
install:
	docker exec -ti ${APP_NAME}api_php_container sh -c "php composer.phar install"

# update vendor dependencies
update:
	docker exec -ti ${APP_NAME}api_php_container sh -c "php composer.phar update"

# enter nginx container
nginx:
	docker exec -ti ${APP_NAME}api_nginx_container sh

# enter php container
php:
	docker exec -ti ${APP_NAME}api_php_container sh

# enter mongo container
mongo:
	docker exec -ti ${APP_NAME}api_mongo_container sh

# enter mongo-express container
mongo-express:
	docker exec -ti ${APP_NAME}api_mongo_express_container sh

# enter jenkins container
jenkins:
	docker exec -ti ${APP_NAME}api_jenkins_container sh

# initialize database
db:
	make db-drop && make db-create && make db-fixtures

# create database
db-create:
	docker exec -ti ${APP_NAME}api_php_container sh -c "php bin/console doctrine:mongodb:schema:create"

# drop database
db-drop:
	docker exec -ti ${APP_NAME}api_php_container sh -c "php bin/console doctrine:mongodb:schema:drop"

# update database
db-update:
	docker exec -ti ${APP_NAME}api_php_container sh -c "php bin/console doctrine:mongodb:schema:update"

# load database fixtures
db-fixtures:
	docker exec -ti ${APP_NAME}api_php_container sh -c "php bin/console doctrine:mongodb:fixtures:load"

# stop all docker containers
stop:
	docker kill $(shell docker ps -aq)

# remove all docker containers
rem-c:
	docker rm $(shell docker ps -aq)

# remove all docker images
rem-i:
	docker rmi $(shell docker images -q)

# remove all docker containers and images
boom:
	make rem-c && make rem-i
